<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//Route::get('/', 'HomeController@index')->name('home');
//
Route::get('/', 'ProductController@productList')->name('home');

Route::get('order', 'OrderController@showOrder')->name('order');

Route::get('addorder', 'OrderController@addOrderPage')->name('addorder');

Route::post('addorder', 'OrderController@entryOrder')->name('entryOrder');

Route::get('edit/{id}', 'OrderController@showEditPage')->name('editOrder');

Route::post('updateorder/{id}', 'OrderController@updateOrder')->name('updateOrder');

Route::get('updateorder/{id}', 'OrderController@deliveryOrder')->name('deliveryOrder');

Route::get('delete/{id}', 'OrderController@deleteOrder')->name('deleteOrder');

Route::get('profile', 'ProfileController@showProfile')->name('profile');

Route::post('profile/{id}', 'ProfileController@updateProfile')->name('updateProfile');

Route::get('approve', 'ApproveController@showApproval')->name('approvePage');

Route::get('approve/{id}', 'ApproveController@approveUser')->name('approveUser');

Route::get('reject/{id}', 'ApproveController@rejectUser')->name('rejectUser');

Route::get('addproduct', 'ProductController@addProductPage')->name('addProduct');

Route::post('addproduct', 'ProductController@insertProduct')->name('insertProduct');

Route::get('detailsproduct/{id}', 'ProductController@detailsProduct')->name('detailsProduct');

Route::post('updateproduct/{id}', 'ProductController@updateProduct')->name('updateProduct');

Route::get('deleteproduct/{id}', 'ProductController@deleteProduct')->name('deleteProduct');
