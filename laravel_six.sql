-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 10, 2018 at 03:04 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_six`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_02_14_215200_create_orders_table', 1),
(4, '2018_02_17_223154_create_products_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(10) UNSIGNED NOT NULL,
  `customer_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_cell` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shoulder` int(11) NOT NULL,
  `put` int(11) NOT NULL,
  `chest` int(11) NOT NULL,
  `length` int(11) NOT NULL,
  `hand` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `payment` int(11) NOT NULL,
  `total_cost` int(11) NOT NULL,
  `delivery_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_order` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `customer_name`, `customer_cell`, `customer_address`, `shoulder`, `put`, `chest`, `length`, `hand`, `quantity`, `payment`, `total_cost`, `delivery_date`, `delivery_order`, `created_at`, `updated_at`) VALUES
(1, 'Kamrul Hasan', '01900000000', 'Shukrabad, Dhanmondi - 32., Narayanganj', 1, 2, 3, 4, 5, 1, 350, 500, '2018-02-21', 0, '2018-02-16 16:57:08', '2018-02-16 16:57:08'),
(5, 'Nurul Islam', '01500000000', 'Block-B, Banani, Dhaka', 1, 2, 3, 4, 5, 1, 740, 1000, '2018-03-29', 0, '2018-03-10 04:35:32', '2018-03-10 04:35:32');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `p_id` int(10) UNSIGNED NOT NULL,
  `product_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` int(11) NOT NULL,
  `product_sell` int(11) NOT NULL,
  `product_manufacture` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`p_id`, `product_id`, `product_name`, `product_image`, `product_price`, `product_sell`, `product_manufacture`, `created_at`, `updated_at`) VALUES
(13, 'P#001', 'Panjabi', '1520689177web-performance-optimization-tools.jpg', 1000, 2, 700, '2018-03-10 07:39:37', '2018-03-10 07:43:06'),
(14, 'P#002', 'T-shirt', '1520689216website-performance-optimization.png', 200, 0, 150, '2018-03-10 07:40:16', '2018-03-10 07:40:16'),
(15, 'P#003', 'Shirt', '1520689297car.png', 700, 1, 500, '2018-03-10 07:41:06', '2018-03-10 07:42:53');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `status`, `password`, `active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'S Ahmed Naim', 'naim.ahmed035@gmail.com', 'Admin', '$2y$10$gz0cws387GL2GCgX.s0cB.hc/.cCrEuEgt4qm5VxxT.10dflvgobq', 1, 'ZyAErNNerARbOKnLRBsU7dvUeSxcTYGroF4Wd288xEbLSZSaTG6P4wT3uQsr', '2018-02-16 12:26:11', '2018-03-10 03:55:13'),
(2, 'Alamgir Hossain', 'alamgir898@gmail.com', 'Sells Person', '$2y$10$YGHs8r9cdMpegv4REgVasOrgg5g5XhPnSwPRJFvxfzJmAfdwbXwi.', 2, 'wFOWyxb4cE6aHsh3mkZ6UkzBcZsHETLgy31pPsEIBaUDSXcYu0ka2vx345dd', '2018-02-16 12:37:07', '2018-02-16 12:37:07'),
(3, 'Nobin Khan', 'nobin@yahoo.com', 'Sells Person', '$2y$10$bXuvxwtOT/Nbom.7zvSf3OvbVLbPvD2AbekdXXknP5MUy.cnPKdDC', 1, 'oIfsZ2pOC81pu7g6Pp9F1RFNKZeh3sAM1wLzeJqCfcaPwwJAkrYF78u5O1qY', '2018-02-16 12:37:50', '2018-03-10 03:59:23'),
(4, 'Nurul Huda Sarker', 'huda@gmail.com', 'Sells Person', '$2y$10$vqRQfBwDc7TsVk9FW7/By.oS/Lf.yEc1C/qNGvcS/S9sGMTrqoFIq', 0, 'sCbMDLspxwNZz92pFBYuJH3fKFHztTkeEEdM7Ra6NvyCeNforBoCn7Qf5wcE', '2018-02-16 12:55:42', '2018-02-16 12:55:42'),
(5, 'Shakil Ahmed', 'shakil@gmail.com', 'Sells Person', '$2y$10$sCTe/eReUm3w3Lrlz8lhxe0MTFF37kW3.CDgEw8gk640Lp9.ERj7G', 0, 'WEcL7CtwOBxAwkmRUL8xsm7c3Y8wsZUHae7fcVOCAjoJNyktOQf09SNcRP4g', '2018-02-16 12:57:02', '2018-02-16 12:57:02'),
(6, 'Faisal Ahmed', 'faisal@gmail.com', 'Customer', '$2y$10$cL2iVHwBN5Lnhlh0l8ezH.cnIERJlEW6dg2oc0vGii.AOYwttogwi', 0, '2I6TQshRZAd99ACEpkxfQkgRQzLK3xOtnjJTZfDlMLMpgV9fCouF13pE9nZy', '2018-02-16 12:57:51', '2018-02-16 13:38:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `p_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
