<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ApproveController extends Controller
{
    public function showApproval()
    {
    	$users = User::orderBy('id', 'DESC')->get();
    	return view('approve', compact('users'));
    }

    public function approveUser($id)
    {
    	$user = User::find($id);
    	$user->active = 1;
    	$user->save();
    	return redirect(route('approvePage'))->with('successMsg', 'User Approved Successfully!');
    }

    public function rejectUser($id)
    {
    	$user = User::find($id);
    	$user->active = 2;
    	$user->save();
    	return redirect(route('approvePage'))->with('successMsg', 'User Rejected Successfully!');
    }
}
