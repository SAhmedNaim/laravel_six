<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use File;

class ProductController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function productList()
    {
        $products = Product::paginate(4);
    	return view('home', compact('products'));
    }

    public function addProductPage($value='')
    {
    	return view('addProduct');
    }

    public function insertProduct(Request $request)
    {
    	$this->validate($request, [
    		'product_id'			=> 'required',
    		'product_name'			=> 'required',
    		'product_manufacture'	=> 'required',
    		'product_price'			=> 'required',
    		'file'					=> 'required'
    	]);

    	if ($request->hasFile('file')) 
        {
    		$fileName 		= $request->file->getClientOriginalName();

    		$uniqueFileName = time().$fileName;

    		$product_image 	= $request->file->storeAs('public/upload', $uniqueFileName);

    		//$product_image = $request->file->store('public/upload');
    		

   			// $fileName = $request->file->getClientOriginalName();
   			// $fileSize = $request->file->getClientSize();

			// $request->file->storeAs('public/upload', $fileName);

			// $product = new Product;


			// $product->name = $fileName;
			// $product->size = $fileSize;

			// $file->save();

			// return 'done';
    	}

    	$product = new Product;
    	$product->product_id			= $request->product_id;
    	$product->product_name			= $request->product_name;

    	$product->product_image			= $uniqueFileName;

    	$product->product_price			= $request->product_price;
    	$product->product_sell			= 0;
    	$product->product_manufacture	= $request->product_manufacture;
    	
    	$product->save();

    	return redirect(route('home'))->with('successMsg', 'Product Added Successfully!!!');
    }

    public function detailsProduct($id)
    {
    	$product = Product::find($id);
    	return view('detailsProduct', compact('product'));
    }

    private function unlinkImageFromFolder($id)
    {
        $product = Product::where('p_id', $id)->first();
        $image = $product->product_image;
        $fileName = public_path().'/storage/upload/'.$image;
        \File::delete($fileName);
    }

    public function updateProduct(Request $request, $id)
    {
        $this->validate($request, [
            'product_id'            => 'required',
            'product_name'          => 'required',
            'product_manufacture'   => 'required',
            'product_price'         => 'required',
            'product_sell'          => 'required'
        ]);

        if ($request->hasFile('file')) 
        {

            $fileName       = $request->file->getClientOriginalName();
            $uniqueFileName = time().$fileName;
            $product_image  = $request->file->storeAs('public/upload', $uniqueFileName);

            $product = Product::where('p_id', $id)->first();

            $this->unlinkImageFromFolder($id);

            $product->product_id            = $request->product_id;
            $product->product_name          = $request->product_name;
            $product->product_manufacture   = $request->product_manufacture;
            $product->product_price         = $request->product_price;
            $product->product_sell          = $request->product_sell;
            $product->product_image         = $uniqueFileName;
            $product->save();

            return redirect(route('detailsProduct', $product->p_id))->with('successMsg', 'Product Updated Successfully!!!');
        }
        else
        {
            $product = Product::where('p_id', $id)->first();
            $product->product_id            = $request->product_id;
            $product->product_name          = $request->product_name;
            $product->product_manufacture   = $request->product_manufacture;
            $product->product_price         = $request->product_price;
            $product->product_sell          = $request->product_sell;
            $product->save();

            return redirect(route('detailsProduct', $product->p_id))->with('successMsg', 'Product Updated Successfully!!!');
        }

    }

    public function deleteProduct($id)
    {
        Product::where('p_id', $id)->delete();
        return redirect(route('home'))->with('successMsg', 'Product Deleted Successfully!!!');
    }

}
