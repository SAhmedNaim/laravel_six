<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;

class OrderController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

    public function showOrder()
    {
    	$orderList = Order::paginate(4);
		return view('order', compact('orderList'));
    }

    public function addOrderPage()
    {
    	return view('addorder');
    }

    public function entryOrder(Request $request)
    {
    	$this->validate($request, [
    		'customer_name'		=> 'required',
    		'customer_cell'		=> 'required',
    		'customer_address' 	=> 'required',
    		'shoulder' 			=> 'required',
    		'put'		 		=> 'required',
    		'chest' 			=> 'required',
    		'length' 			=> 'required',
    		'hand' 				=> 'required',
    		'quantity' 			=> 'required',
    		'payment' 			=> 'required',
    		'total_cost' 		=> 'required',
    		'delivery_date' 	=> 'required'
    	]);

    	$order = new Order;
    	$order->customer_name 		= $request->customer_name;
    	$order->customer_cell 		= $request->customer_cell;
    	$order->customer_address 	= $request->customer_address;
    	$order->shoulder 			= $request->shoulder;
    	$order->put 				= $request->put;
    	$order->chest 				= $request->chest;
    	$order->length 				= $request->length;
    	$order->hand 				= $request->hand;
    	$order->quantity 			= $request->quantity;
    	$order->payment 			= $request->payment;
    	$order->total_cost 			= $request->total_cost;
    	$order->delivery_date 		= $request->delivery_date;
    	$order->save();

    	return redirect(route('order'))->with('successMsg', 'Order Added Successfully!!!');
    }

    public function showEditPage($id)
    {
    	$order = Order::where('order_id', $id)->first();
    	return view('detailsorder', compact('order'));
    }

    public function updateOrder(Request $request, $id)
    {
    	$this->validate($request, [
    		'customer_name'		=> 'required',
    		'customer_cell'		=> 'required',
    		'customer_address' 	=> 'required',
    		'shoulder' 			=> 'required',
    		'put'		 		=> 'required',
    		'chest' 			=> 'required',
    		'length' 			=> 'required',
    		'hand' 				=> 'required',
    		'quantity' 			=> 'required',
    		'payment' 			=> 'required',
    		'total_cost' 		=> 'required',
    		'delivery_date' 	=> 'required'
    	]);

    	$order = Order::where('order_id', $id)->first();
    	$order->customer_name 		= $request->customer_name;
    	$order->customer_cell 		= $request->customer_cell;
    	$order->customer_address 	= $request->customer_address;
    	$order->shoulder 			= $request->shoulder;
    	$order->put 				= $request->put;
    	$order->chest 				= $request->chest;
    	$order->length 				= $request->length;
    	$order->hand 				= $request->hand;
    	$order->quantity 			= $request->quantity;
    	$order->payment 			= $request->payment;
    	$order->total_cost 			= $request->total_cost;
    	$order->delivery_date 		= $request->delivery_date;
    	$order->save();

    	//return redirect(route('order'))->with('successMsg', 'Order Updated Successfully!!!');
    	return redirect(route('editOrder', $order->order_id))->with('successMsg', 'Order Updated Successfully!!!');
    }

    public function deliveryOrder($id)
    {
    	$order = Order::where('order_id', $id)->first();
    	$order->delivery_order = '1';
    	$order->save();
    	return redirect(route('editOrder', $order->order_id))->with('successMsg', 'Order Delivered Successfully!!!');
    }

    public function deleteOrder($id)
    {
        Order::where('order_id', $id)->delete();
        return redirect(route('order'))->with('successMsg', 'Order Deleted Successfully!!!');
    }

}
