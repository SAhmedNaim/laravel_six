<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class ProfileController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function showProfile()
    {
    	$currentUser = Auth::id();
    	$user = User::find($currentUser);
    	return view('profile', compact('user'));
    }

    public function updateProfile(Request $request, $id)
    {
    	$this->validate($request, [
    		'username'	=> 'required',
    		'email'		=> 'required'
    	]);

    	$user = User::find($id);

    	$user->username = $request->username;
    	$user->email	= $request->email;
    	$user->status	= $user->status;

    	$user->save();

    	return redirect(route('profile'))->with('successMsg', 'User Updated Successfully!');
    }

}
