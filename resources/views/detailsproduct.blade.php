@extends('layouts.app')

@section('content')

<div class="panel-body">
    
    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-dismissible alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Oh snap!</strong>
                {{ $error }}
            </div>
        @endforeach
    @endif
    
    @if(session('successMsg'))
        <div class="alert alert-dismissible alert-success">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>Well done!</strong> {{ session('successMsg') }}
        </div>
    @endif

	<!-- default navbar goes here -->
	<nav class="navbar navbar-default">
		  <div class="container-fluid">
			    <div class="navbar-header">
				      <span class="navbar-brand">
                <h4><a style="margin-top: -10px;" href="{{ route('home') }}" class="btn btn-default">Go to Home</a></h4>
            </span>
			    </div>
			    <ul class="nav navbar-nav pull-right">
				      <li><a><h4>Admin</h4></a></li>
			    </ul>
		  </div>
	</nav>
	
	<!-- information table goes here -->
	<form method="POST" action="{{ route('updateProduct', $product->p_id) }}" enctype="multipart/form-data">
        
        {{ csrf_field() }}

        <div class="form-group">
            <label for="productID">Product ID</label>
            <input type="text" class="form-control" id="productID" name="product_id" value="{{ $product->product_id }}" />
        </div>
        <div class="form-group">
            <label for="productName">Product Name</label>
            <input type="text" class="form-control" id="productName" name="product_name" value="{{ $product->product_name }}"/>
        </div>
        <div class="form-group">
            <label for="productManufacturingCost">Manufacturing Cost</label>
            <input type="number" class="form-control" id="exampleInputEmail1" name="product_manufacture" value="{{ $product->product_manufacture }}"/>
        </div>
        <div class="form-group">
            <label for="productPrice">Product Price</label>
            <input type="number" class="form-control" id="productPrice" name="product_price" value="{{ $product->product_price }}"/>
        </div>
        <div class="form-group">
            <label for="productSell">Product Sell</label>
            <input type="number" class="form-control" id="productSell" name="product_sell" value="{{ $product->product_sell }}"/>
        </div>
        <div class="form-group">
            <label for="productImage">Product Image</label>
            <br/>
            <img src="{{ asset('storage/upload/'.$product->product_image) }}" alt="Product image missing!" width="100%" height="400px" style="padding-bottom: 7px;" />
            <input type="file" id="productImage" name="file" />
            <p class="help-block">Click "Choose File" button to upload product image</p>
        </div>
        <button type="submit" class="btn btn-default" name="updateProduct">Update Product</button>
    </form>
</div>

@endsection