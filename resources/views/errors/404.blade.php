<!DOCTYPE HTML>
<html>
	<head>
		<title></title>
		<link href="{{ asset('404/style.css') }}" rel="stylesheet" type="text/css"  media="all" />
	</head>
	<body>
		<!--start-wrap--->
		<div class="wrap">
			<!---start-header---->
				<div class="header">
					<div class="logo">
						<h1><a href="{{ route('home') }}">Ohh</a></h1>
					</div>
				</div>
				<!---728x90--->
			<!---End-header---->
			<!--start-content------>
			<div class="content">
				<img src="{{ asset('404/error-img.png') }}" title="error" />
				<!---728x90--->
				<p><span><label>O</label>hh.....</span>You Requested the page that is no longer There.</p>
				
				<a href="{{ route('home') }}">Back To Home</a>
				<!---728x90--->
				<div class="copy-right">
					<p>&copy; {{ @date('Y') }} Ohh. All Rights Reserved | Designed, Developed & Maintained by <a href="http://itinfokit.com/">S Ahmed Naim</a></p>
				</div>
   			</div>
			<!--End-Cotent------>
		</div>
		<!--End-wrap--->
	</body>
</html>

