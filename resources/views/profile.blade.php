@extends('layouts.app')

@section('content')

<div class="panel-body">
    
@if(session('successMsg'))
    <div class="alert alert-dismissible alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Success!</strong> {{ @session('successMsg') }}
    </div>
@endif

@if($errors->any())
    @foreach($errors->all() as $error)
        <div class="alert alert-dismissible alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>Oh Snap! </strong> {{ $error }}
        </div>
    @endforeach
@endif

	<!-- default navbar goes here -->
	<nav class="navbar navbar-default">
	    <div class="container-fluid">
		    <div class="navbar-header">
			  	<span class="navbar-brand">
                    <h4>User Profile</h4>
                </span>
			</div>
		    <ul class="nav navbar-nav pull-right">
			    <li><a><h4><a style="margin-top: -30px;" href="{{ route('home') }}" class="btn btn-default">Go to Home</a></h4></a></li>
		    </ul>
		</div>
	</nav>

	<!-- information table goes here -->
	<form method="POST" action="{{ route('updateProfile', $user->id) }}">

        {{ csrf_field() }}
    
        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" class="form-control" id="username" name="username" value="{{ $user->username }}" />
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}" />
        </div>
        <div class="form-group">
            <label for="status">Status</label>
            <input type="text" class="form-control" id="status" name="status" value="{{ $user->status }}" disabled />
        </div>

        <button type="submit" class="btn btn-default" name="submit">Update Profile</button>
    </form>

</div>

@endsection