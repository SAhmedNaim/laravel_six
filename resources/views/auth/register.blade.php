@extends('layouts.app')

@section('content')

<div class="panel-heading">
    <h2>User Registration</h2>
</div>

<div class="panel-body">
    <div class="row">
        <div class="col-md-4">
            <img src="{{ asset('img/logo.jpg') }}" alt="Company Logo" style="height: 335px; width: 350px;" />
        </div>
        <div class="col-md-8">
            <div style="max-width: 600px; margin: 0px auto;">
                <form method="POST" action="{{ route('register') }}">

					{{ csrf_field() }}

                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                        <label for="username">Username</label>
                        <input type="text" id="username" name="username" class="form-control" placeholder="Type username" style="min-width:100%" value="{{ old('username') }}" autofocus />

						@if ($errors->has('username'))
                            <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif

                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">Email Address</label>
                        <input type="text" id="email" name="email" class="form-control" placeholder="Type email" style="min-width:100%" value="{{ old('email') }}" autofocus />

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif

                    </div>
                    
                    <!-- status -->
                    <label for="status" class="{{ $errors->has('email') ? ' has-error' : '' }}">Status</label>
                    <select name="status" class="form-control" value="{{ old('email') }}" autofocus>
                        <option value="" selected="">Choose your status</option>
                        <option value="Sells Person">Sells Person</option>
                        <option value="Customer">Customer</option>
                    </select>
                	@if ($errors->has('status'))
                        <span class="help-block">
                            <strong>{{ $errors->first('status') }}</strong>
                        </span>
                    @endif
                    <br/>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password">Password</label>
                        <input type="password" id="password" name="password" class="form-control" placeholder="Type password" style="min-width:100%" />
                        @if ($errors->has('password'))
	                        <span class="help-block">
	                            <strong>{{ $errors->first('password') }}</strong>
	                        </span>
	                    @endif
                    </div>

					<div class="form-group{{ $errors->has('active') ? ' has-error' : '' }}" style="display: none;">
                        <label for="active">Active</label>
                        <input type="text" id="active" name="active" class="form-control" placeholder="Type active" style="min-width:100%" value="0" autofocus />

						@if ($errors->has('active'))
                            <span class="help-block">
                                <strong>{{ $errors->first('active') }}</strong>
                            </span>
                        @endif

                    </div>

                    <button type="submit" name="register" class="btn btn-default">Register</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection