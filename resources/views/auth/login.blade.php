@extends('layouts.app')

@section('content')

<div class="panel-heading">
    <h2>User Login</h2>
</div>

<div class="panel-body">

    <div class="row">
        <div class="col-md-4">
            <img src="{{ asset('img/logo.jpg') }}" class="img-responsive" alt="Company Logo" style="height: 180px; width: 350px;" />
        </div>
        <div class="col-md-8">
            <div style="max-width: 600px; margin: 0px auto;">
                <form method="POST" action="{{ route('login') }}">

                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">Email Address</label>
                        <input type="text" id="email" name="email" class="form-control" placeholder="Email" style="min-width:100%" value="{{ old('email') }}" autofocus />

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif

                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password">Password</label>
                        <input type="password" id="password" name="password" class="form-control" placeholder="Password" style="min-width:100%" />

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif

                    </div>
                    <button type="submit" name="login" class="btn btn-default" style="min-width: 15%;">Login</button>
                </form>
            </div>
        </div>
    </div>
</div>



@endsection
