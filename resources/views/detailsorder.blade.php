@extends('layouts.app')

@section('content')

<div class="panel-body">

	@if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-dismissible alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Oh snap!</strong>
                {{ $error }}
            </div>
        @endforeach
    @endif
	
	@if(session('successMsg'))
		<div class="alert alert-dismissible alert-success">
		    <button type="button" class="close" data-dismiss="alert">×</button>
		    <strong>Well done!</strong> {{ session('successMsg') }}
		</div>
	@endif

    <!-- default navbar goes here -->
    <nav class="navbar navbar-default">
	    <div class="container-fluid">
		    <div class="navbar-header">
			    <span class="navbar-brand">
                    <h4><a style="margin-top: -10px;" href="{{ route('order') }}" class="btn btn-default">Go Order Page</a></h4>
                </span>
			</div>
  		    <ul class="nav navbar-nav pull-right">
	    		<li><a><h4>Admin</h4></a></li>
		    </ul>
        </div>
    </nav>


<!-- information table goes here -->
<form method="POST" action="{{ route('updateOrder', $order->order_id) }}">
	
	{{ csrf_field() }}

    <div class="form-group">
        <label for="customerName">Customer Name</label>
        <input type="text" class="form-control" id="customerName" name="customer_name" value="{{ $order->customer_name }}" />
    </div>
    <div class="form-group">
        <label for="cell">Customer Cell</label>
        <input type="number" class="form-control" id="cell" name="customer_cell" value="{{ $order->customer_cell }}" />
    </div>
    <div class="form-group">
        <label for="address">Address</label>
        <input type="text" class="form-control" id="address" name="customer_address" value="{{ $order->customer_address }}" />
    </div>
    <div class="form-group">
        <label for="shoulder">Shoulder</label>
        <input type="number" class="form-control" id="shoulder" name="shoulder" value="{{ $order->shoulder }}" />
    </div>
    <div class="form-group">
        <label for="put">Put</label>
        <input type="number" class="form-control" id="put" name="put" value="{{ $order->put }}" />
    </div>
    <div class="form-group">
        <label for="chest">Chest</label>
        <input type="number" class="form-control" id="chest" name="chest" value="{{ $order->chest }}" />
    </div>
    <div class="form-group">
        <label for="length">Length</label>
        <input type="number" class="form-control" id="length" name="length" value="{{ $order->length }}" />
    </div>
    <div class="form-group">
        <label for="hand">Hand</label>
        <input type="number" class="form-control" id="hand" name="hand" value="{{ $order->hand }}" />
    </div>
    <div class="form-group">
        <label for="quantity">Quantity</label>
        <input type="number" class="form-control" id="quantity" name="quantity" value="{{ $order->quantity }}" />
    </div>
    <div class="form-group">
        <label for="advance">Payment</label>
        <input type="number" class="form-control" id="advance" name="payment" value="{{ $order->payment }}" />
    </div>
    <div class="form-group">
        <label for="due">Due</label>
        <input type="number" class="form-control" id="due" name="due" 
        value="{{ $order->total_cost - $order->payment }}"
        />
    </div>
    <div class="form-group">
        <label for="totalCost">Total</label>
        <input type="number" class="form-control" id="totalCost" name="total_cost" value="{{ $order->total_cost }}" />
    </div>
    <div class="form-group">
        <label for="date">Delivery Date</label>
        <input type="date" class="form-control" id="date" name="delivery_date" value="{{ $order->delivery_date }}" />
    </div> 
    <button type="submit" class="btn btn-default" name="updateOrder">Update Order</button>
    {{-- <button type="submit" class="btn btn-default" name="deliveryOrder">Delivery Order</button> --}}
    <a href="{{ route('deliveryOrder',  $order->order_id) }}" class="btn btn-default">Delivery Order</a>
</form>
</div>

@endsection