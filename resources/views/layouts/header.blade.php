<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>S I X</title>

        <!-- Bootstrap -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('css/style.css') }}" rel="stylesheet"/>
        <link rel="icon" href="{{ asset('img/logo.jpg') }}"/>
        
        <!-- jquery -->
        <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    
    <body>

        <div class="container">
            <!-- default navbar code goes here -->
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{ route('home') }}"><b><img src="{{ asset('img/logo.jpg') }}" style="width: 130px; height: 30px; margin-top: -10px;"></b></a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav"></ul>
            
                        <ul class="nav navbar-nav navbar-right">
                            
                            @if(Auth::guest())
                                <li role="presentation"><a href="{{ route('login') }}"><span aria-hidden="true"></span> Login</a></li>
                                <li role="presentation"><a href="{{ route('register') }}"><span aria-hidden="true"></span> Register</a></li> 
                            @else

                                <li role="presentation"><a href="{{ route('order') }}" target="_blank"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Order</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        
                                        {{ Auth::user()->username }}
                                        
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ route('profile') }}" target="_blank"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Profile</a></li>

                                        <li role="separator" class="divider"></li>
                                        
                                        
                                                <li><a href="{{ route('approvePage') }}" target="_blank">
                                                    <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span> Approval Setting</a>
                                                </li> 

                                        <li>
                                            <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Logout
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>

                            @endif


                </ul>
                
                </div><!-- /.container-fluid collapse -->
           </nav>
      <!-- default navbar just collapse -->

<div class="panel panel-default">
