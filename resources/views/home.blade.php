@extends('layouts.app')

@section('content')

<div class="panel-body">
    <!-- default navbar goes here -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <span class="navbar-brand"><h4><a style="margin-top: -10px;" href="{{ route('addProduct') }}" target="_blank" class="btn btn-default">Add New Product</a></h4></span>
            </div>
            <ul class="nav navbar-nav pull-right" style="width: 40%;">
                <li><a>

                    <!-- Searchbar code goes here --><!--                                           
                    <input type="text" name="" placeholder="Seach product here. . ." style="width: 290px !important; margin: 5px;">-->
                    <form action="search.php" method="post">    
                        <div class="col-lg-6">
                            <div class="input-group" style=" margin-bottom: 15px;">
                                <input type="text" class="form-control" name="keyword" placeholder="Search product here. . ." style="width:320px !important;" />
                                <span class="input-group-btn">
                                    <input type="submit" class="btn btn-default" name="search" value="Search" />
                                </span>
                            </div><!-- /input-group -->
                        </div><!-- /.col-lg-6 -->
                    </form>
                    <!-- Searchbar code collapse -->
                    
                </a></li>
            </ul>
        </div>
    </nav>
    
    <!-- information table goes here -->
    <table class="table table-striped table-bordered">
        <th width="10%">Product ID</th>
        <th width="20%">Product Name</th>
        <th width="20%">Product Image</th>
        <th width="10%">Price</th>
        <th width="10%">Total Sell</th>
        <th width="10%">Action</th>
        
    @foreach($products as $product)            

        <tr>
            <td>{{ $product->product_id }}</td>
            <td>{{ $product->product_name }}</td>
            <td><img src="{{ asset('storage/upload/'.$product->product_image) }}" width="200px" height="30px" alt="Product Image Missing"></td>
            <td>{{ $product->product_price }}</td>
            <td>{{ $product->product_sell }}</td>
            <td>
                <!--<a class="btn btn-primary" href="profile.php?id=1">View</a>-->
                <!-- Split button -->
                <div class="btn-group">
                  <button type="button" class="btn btn-default">Action</button>
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu">
                    <li>
                        <a href="{{ route('detailsProduct', $product->p_id) }}" target="_blank"><span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span> Details</a>
                    </li>
                    <li>
                        <a href="{{ route('deleteProduct', $product->p_id) }}" onclick="return confirm('Are you sure you want to delete data?');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a>
                    </li>
                                    
                  </ul>
                </div>
            </td>
        </tr> 

    @endforeach

    @if(empty($products->all()))
        <tr>
            <td colspan="6" style="letter-spacing: 4px;"><h2>Product Not Found</h2></td>
        </tr> 
    @endif

    </table>

    <div class="col-md-4 col-lg-4"></div>
    <div class="col-md-4 col-lg-4" style="text-align:center;">
        <!-- Pagination -->
        {{ $products->links() }}
    </div>
    <div class="col-md-4 col-lg-4"></div>
    

</div>

@endsection