@extends('layouts.app')

@section('content')

<div class="panel-body">

	@if(session('successMsg'))
		<div class="alert alert-dismissible alert-success">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>Success!</strong> {{ session('successMsg') }}
        </div>
	@endif

	<!-- default navbar goes here -->
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<span class="navbar-brand"><h4>User List</h4></span>
			</div>
			<ul class="nav navbar-nav pull-right" style="width: 40%;">
				<li><a>
					
					<div class="col-lg-6">
					    <div class="input-group" style=" margin-bottom: 15px;">
					      	<input type="text" class="form-control" placeholder="Search user here. . ." style="width:320px !important;">
					      	<span class="input-group-btn">
					        	<button class="btn btn-default" type="button">Search</button>
					     	</span>
					    </div><!-- /input-group -->
					</div><!-- /.col-lg-6 -->

				</a></li>
			</ul>
		</div>
	</nav>
	
	<!-- information table goes here -->
	<table class="table table-striped table-bordered">
		<th width="10%">Serial</th>
		<th width="20%">Username</th>
		<th width="30%">Email</th>
		<th width="20%">Status</th>
		<th width="20%">Approval</th>
		
		@php
			$i = 0;
		@endphp

		@foreach($users as $user)

		<tr>
			<td>{{ $i = $i + 1 }}</td>
			<td>{{ $user->username }}</td>
			<td>{{ $user->email }}</td>
			<td>{{ $user->status }}</td>
			<td>
				<!--<a class="btn btn-primary" href="profile.php?id=1">View</a>-->
				<!-- Split button -->
				<div class="btn-group">
				  <button type="button" class="btn btn-default">

				 		@if($user->active == 0)
							Pending
						@elseif(($user->active == 1) && ($user->id == Auth::user()->id))
							Active
						@elseif($user->active == 1)
							Approved
						@elseif($user->active == 2)
							Rejected
						@endif

				  </button>
				  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    <span class="caret"></span>
				    <span class="sr-only">Toggle Dropdown</span>
				  </button>
				  <ul class="dropdown-menu">
						
						@if($user->active == 0)	
							<li>
					    		<a href="{{ route('approveUser', $user->id) }}" onclick="return confirm('Are you sure to approve?');"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Approve</a>
					    	</li>
					  		<li>
					  			<a href="{{ route('rejectUser', $user->id) }}" onclick="return confirm('Are you sure to reject user?');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Reject</a>
					    	</li> 
						@elseif(($user->active == 1) && ($user->id == Auth::user()->id))
							<li>
								<a href="{{ route('profile') }}" target="_blank"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Profile</a>
							</li> 
						@elseif($user->active == 1)
							<li>
			  					<a href="{{ route('rejectUser', $user->id) }}" onclick="return confirm('Are you sure to reject user?');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Reject</a>
			    			</li> 
			    		@elseif($user->active == 2)
					    	<li>
					    		<a href="{{ route('approveUser', $user->id) }}" onclick="return confirm('Are you sure to approve?');"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Approve</a>
					    	</li> 
			  			@endif

				  </ul>
				</div>
			</td>
		</tr> 

		@endforeach

	</table>

</div>

@endsection	