@extends('layouts.app')

@section('content')

<div class="panel-body">

    @if(Session('successMsg'))
        <div class="alert alert-dismissible alert-success">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>Well done!</strong> {{ session('successMsg') }}
        </div>
    @endif

    <!-- default navbar goes here -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <span class="navbar-brand"><h4><a style="margin-top: -10px;" href="{{ route('addorder') }}" target="_blank" class="btn btn-default">Add New Order</a></h4></span>
            </div>
            <ul class="nav navbar-nav pull-right" style="width: 10%; margin-top: -10px; padding-bottom: 0px;">

            <span class="navbar-brand"><h4><a style="margin-right: 00px; " href="{{ route('home') }}" class="btn btn-default">Go to Home</a></h4></span>

                <li><a>

                </a></li>
            </ul>
        </div>
    </nav>
    
    <!-- information table goes here -->
    <table class="table table-striped table-bordered">
        <th width="10%">Serial</th>
        <th width="20%">Customer Name</th>
        <th width="10%">Payment</th>
        <th width="10%">Due</th>
        <th width="10%">Total Cost</th>
        <th width="10%">Delivery Date</th>
        <th width="10%">Action</th>


    @if($orderList)

        @php
        $i = $orderList->perPage() * ($orderList->currentPage()-1);
        @endphp
            
        @foreach($orderList as $list)

        <tr>
            <td>{{ $i = $i + 1 }}</td>
            <td>{{ $list->customer_name }}</td>
            <td>{{ $list->payment }}</td>
            <td>
                {{ $list->total_cost - $list->payment }}
            </td>
            <td>{{ $list->total_cost }}</td>
            <td>{{ $list->delivery_date }}</td>
            <td>
                <!--<a class="btn btn-primary" href="profile.php?id=1">View</a>-->
                <!-- Split button -->
                <div class="btn-group">
                    <button type="button" class="btn btn-default">
                        
                        @if($list->delivery_order == 0)
                            Pending
                        @else
                            Delivered
                        @endif
                        
                    </button>
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ route('editOrder', $list->order_id) }}"target="_blank"><span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span> Details</a>
                        </li>
                        <li>
                            <a href="{{ route('deleteOrder', $list->order_id) }}" onclick="return confirm('Are you sure to delete data?');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a>
                        </li>
                                    
                  </ul>
                </div>
            </td>
        </tr> 

        @endforeach
    
    @else

        <tr>
            <td colspan="7" style="letter-spacing: 4px;"><h2>Order Not Found</h2></td>
        </tr>

    @endif

    </table>

    <div class="col-md-4 col-lg-4"></div>
    <div class="col-md-4 col-lg-4" style="text-align:center;">
        <!-- Pagination -->
        {{ $orderList->links() }}
    </div>
    <div class="col-md-4 col-lg-4"></div>

</div>

@endsection