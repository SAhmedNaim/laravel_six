<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('order_id');
            $table->string('customer_name');
            $table->string('customer_cell');
            $table->string('customer_address');
            $table->integer('shoulder');
            $table->integer('put');
            $table->integer('chest');
            $table->integer('length');
            $table->integer('hand');
            $table->integer('quantity');
            $table->integer('payment');
            $table->integer('total_cost');
            $table->string('delivery_date');
            $table->integer('delivery_order')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
